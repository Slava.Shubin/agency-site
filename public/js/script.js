$(document).ready(function(){
  			$('.slider').slick({
  				arrows: false,
    			dots: true,
    			infinite: true,
    			slidesToShow: 4,
    			slidesToScroll: 2,
    			autoplay: true,
    			responsive: [
    				{
    					breakpoint: 1024,
      					settings: {
				        slidesToShow: 3,
				        slidesToScroll: 2,
				        infinite: true,
				        dots: true
    					}
    				},
    				{
				      	breakpoint: 600,
				      	settings: {
				        slidesToShow: 2,
				        slidesToScroll: 1
				      }
				    },
				    {
				      	breakpoint: 480,
				      	settings: {
				        slidesToShow: 1,
				        slidesToScroll: 1
				      }
				    }
    			]
  			});
		});